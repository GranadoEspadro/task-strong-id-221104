#! /usr/bin/env bash

# Install Nginx
echo "----- Install Nginx ------"
sudo apt update
sudo apt install nginx -y
sudo systemctl start nginx
sudo systemctl enable nginx
sudo systemctl status nginx

# Install Prometheus
echo "----- Install Prometheus ------"
wget https://github.com/prometheus/prometheus/releases/download/v2.37.1/prometheus-2.37.1.linux-amd64.tar.gz
sudo tar xvf ./prometheus-2.37.1.linux-amd64.tar.gz
sudo cp -r ./prometheus-2.37.1.linux-amd64/prometheus /usr/local/bin
sudo cp -r ./prometheus-2.37.1.linux-amd64/promtool /usr/local/bin
sudo mkdir /etc/prometheus
sudo cp -r ./prometheus-2.37.1.linux-amd64/console_libraries /etc/prometheus/
sudo cp -r ./prometheus-2.37.1.linux-amd64/consoles /etc/prometheus/

cat > /etc/prometheus/prometheus.yml <<EOF
global:
  scrape_interval: 15s
  evaluation_interval: 15s
scrape_configs:
  - job_name: 'prometheus'
    scrape_interval: 15s
    static_configs:
      - targets: ['localhost:9090']
  - job_name: 'node_exporter'
    scrape_interval: 5s
    static_configs:
      - targets: ['192.168.60.10:9100']
EOF

# Create User Prometheus
echo "----- Create User Prometheus------"
sudo useradd --no-create-home --shell /bin/false prometheus

# Start Service Prometheus
echo "----- Start Service Prometheus------"
sudo cat > /etc/systemd/system/prometheus.service <<EOF
[Unit]
Description=Prometheus
Wants=network-online.target
After=network-online.target

[Service]
User=prometheus
Group=prometheus
Type=simple
ExecStart=/usr/local/bin/prometheus \
--config.file /etc/prometheus/prometheus.yml \
--storage.tsdb.path /var/lib/prometheus/ \
--web.console.templates=/etc/prometheus/consoles \
--web.console.libraries=/etc/prometheus/console_libraries

[Install]
WantedBy=multi-user.target
EOF

sudo mkdir /var/lib/prometheus
sudo chown -R prometheus:prometheus /var/lib/prometheus

sudo systemctl start prometheus
sudo systemctl status prometheus

# Install Grafana
echo "----- Install Grafana ------"
sudo apt install apt-transport-https software-properties-common -y
sudo wget -q -O /usr/share/keyrings/grafana.key https://packages.grafana.com/gpg.key
sudo echo "deb [signed-by=/usr/share/keyrings/grafana.key] https://packages.grafana.com/oss/deb stable main" | sudo tee -a /etc/apt/sources.list.d/grafana.list

sudo apt update
sudo apt install grafana -y
sudo systemctl start grafana-server.service
sudo systemctl enable grafana-server.service
sudo systemctl status grafana-server.service


# Install Go
echo "----- Install Go ------"
cd
sudo apt install golang-go -y

# Install Loki
echo "----- Install Loki ------"
cd /usr/src/
sudo git clone https://github.com/grafana/loki
sudo chmod -R 777 /usr/src/loki/cmd
sudo chown -R vagrant:vagrant /usr/src/loki/cmde
cd /usr/src/loki/cmd/loki/ && go build
sudo mv loki /usr/local/bin/
sudo mkdir /etc/loki
sudo mv /usr/src/loki/cmd/loki/loki-local-config.yaml /etc/loki/
sudo sed -i 's/\/tmp\/wal/\/opt\/loki\/wal/g' /etc/loki/loki-local-config.yaml
sudo sed -i 's/\/tmp\/loki/\/opt\/loki/g' /etc/loki/loki-local-config.yaml
sudo mkdir /opt/loki
sudo useradd --no-create-home --shell /bin/false loki
sudo chown loki:loki /usr/local/bin/loki
sudo chown -R loki:loki /etc/loki
sudo chown -R loki:loki /opt/loki

sudo cat > /etc/systemd/system/loki.service <<EOF
[Unit]
Description=Grafana Loki Service
After=network.target

[Service]
User=loki
Group=loki
Type=simple
ExecStart=/usr/local/bin/loki -config.file=/etc/loki/loki-local-config.yaml
ExecReload=/bin/kill -HUP \$MAINPID
Restart=on-failure

[Install]
WantedBy=multi-user.target
EOF

sudo systemctl enable loki
sudo systemctl start loki
sudo systemctl status loki
