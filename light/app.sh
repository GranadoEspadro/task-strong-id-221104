#! /usr/bin/env bash

# Install Nginx
echo "----- Install Nginx ------"
sudo apt update
sudo apt install nginx -y
sudo systemctl start nginx
sudo systemctl enable nginx
sudo systemctl status nginx

# Install Docker
echo "----- Install Docker ------"
sudo apt -y update
sudo apt -y install apt-transport-https ca-certificates curl gnupg-agent software-properties-common
sudo apt remove docker docker-engine docker.io containerd runc
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/trusted.gpg.d/docker-archive-keyring.gpg
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
sudo apt update
sudo apt install docker-ce docker-ce-cli containerd.io -y
sudo usermod -aG docker vagrant

sudo systemctl start docker
sudo systemctl enable docker
sudo systemctl status docker

#Install Docke-Compose
sudo curl -L "https://github.com/docker/compose/releases/download/1.26.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
docker-compose --version

# Docker Build
echo "----- Docker Build ------"
sudo mkdir /app
cd /app && sudo git clone https://gitfront.io/r/deusops/B2vPJxQWdXLf/laravel-monitoring.git
sudo cp /app/laravel-monitoring/.env.example /app/laravel-monitoring/.env

sudo cat > /app/laravel-monitoring/sidecar.sh <<EOF
#!/bin/sh

docker exec --user root travellist_app composer install
docker exec --user root travellist_app php artisan key:generate

EOF

sudo cat > /etc/systemd/system/docker-compose.yml <<EOF
version: "3.7"
services:
  app:
    build:
      context: ./
      dockerfile: Dockerfile
    image: travellist
    container_name: travellist_app
    restart: unless-stopped
    working_dir: /var/www/
    volumes:
      - ./:/var/www
    networks:
      - travellist
    healthcheck:
      test: ["CMD", "php-fpm-healthcheck"]
      timeout: 10s
      retries: 5

  db:
    image: mysql:8.0
    container_name: travellist_db
    restart: unless-stopped
    environment:
      MYSQL_DATABASE: \${DB_DATABASE}
      MYSQL_ROOT_PASSWORD: \${DB_PASSWORD}
      MYSQL_PASSWORD: \${DB_PASSWORD}
      MYSQL_USER: \${DB_USERNAME}
      SERVICE_TAGS: build
      SERVICE_NAME: mysql
    volumes:
      - ./docker-compose/mysql:/docker-entrypoint-initdb.d
    networks:
      - travellist
    healthcheck:
      test: ["CMD", "mysqladmin" ,"ping", "-h", "localhost"]
      timeout: 10s
      retries: 5

  nginx:
    image: nginx:alpine
    container_name: travellist_nginx
    restart: unless-stopped
    ports:
      - 8000:80
    volumes:
      - ./:/var/www
      - ./docker-compose/nginx:/etc/nginx/conf.d/
    networks:
      - travellist
    healthcheck:
      test: curl --fail http://localhost:8000 || exit 1
      interval: 30s
      retries: 5
      start_period: 10s
      timeout: 10s

  sidecar:
    build:
      context: .
      dockerfile: Dockerfile_sidecar
    container_name: travellist-sidecar
    restart: "no"
    command: sh ./sidecar.sh
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock
    networks:
      - travellist
    depends_on:
      - app


networks:
  travellist:
    driver: bridge

EOF

cd /app/laravel-monitoring/ && docker-compose up -d


# Install Promtail
echo "----- Install Promtail ------"

sudo apt install unzip -y
cd
sudo wget https://github.com/grafana/loki/releases/latest/download/promtail-linux-amd64.zip
sudo unzip promtail-linux-amd64.zip
sudo mv promtail-linux-amd64 /usr/local/bin/promtail
sudo mkdir /etc/promtail

sudo cat > /etc/promtail/promtail.yaml <<EOF
server:
  http_listen_port: 9080
  grpc_listen_port: 0

positions:
  filename: /tmp/positions.yaml

clients:
  - url: http://192.168.60.11:3100/loki/api/v1/push

scrape_configs:
- job_name: nginx
  static_configs:
  - targets:
      - localhost
    labels:
      job: nginxlogs
      __path__: /var/log/nginx/*log
  pipeline_stages:
    - match:
        selector: '{job="nginxlogs"}'
        stages:
        - regex:
            expression: '^(?P<remote_addr>[\w\.]+) - (?P<remote_user>[^ ]*) \[(?P<time_local>.*)\] "(?P<method>[^ ]*) (?P<request>[^ ]*) (?P<protocol>[^ ]*)" (?P<status>[\d]+) (?P<body_bytes_sent>[\d]+) "(?P<http_referer>[^"]*)" "(?P<http_user_agent>[^"]*)"?'
        - labels:
            remote_addr:
            remote_user:
            time_local:
            method:
            request:
            protocol:
            status:
            body_bytes_sent:
            http_referer:
            http_user_agent:
EOF

sudo cat > /etc/systemd/system/promtail.service <<EOF
[Unit]
Description=Promtail Service
After=network.target

[Service]
Type=simple
ExecStart=/usr/local/bin/promtail -config.file=/etc/promtail/promtail.yaml
ExecReload=/bin/kill -HUP \$MAINPID
Restart=on-failure

[Install]
WantedBy=multi-user.target
EOF

sudo systemctl enable promtail
sudo systemctl start promtail
sudo systemctl status promtail

# Install Node Exporter
echo "----- Install Node Exporter ------"

cd
wget https://github.com/prometheus/node_exporter/releases/download/v1.4.0-rc.0/node_exporter-1.4.0-rc.0.linux-amd64.tar.gz
tar -xvf ./node_exporter-1.4.0-rc.0.linux-amd64.tar.gz
cp -r ./node_exporter-1.4.0-rc.0.linux-amd64/ /usr/local/bin/node_exporter
sudo useradd --no-create-home --shell /bin/false node_exporter

sudo cat > /etc/systemd/system/node_exporter.service <<EOF
[Unit]
Description=Prometheus Node Exporter
Wants=network-online.target
After=network-online.target

[Service]
User=node_exporter
Group=node_exporter
Type=simple
ExecStart=/usr/local/bin/node_exporter/node_exporter

[Install]
WantedBy=multi-user.target
EOF

chown -R node_exporter:node_exporter /usr/local/bin/node_exporter

systemctl daemon-reload
systemctl enable node_exporter
systemctl start node_exporter
systemctl status node_exporter




